﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace manageHomework.Migrations
{
    /// <inheritdoc />
    public partial class addRoleAdmin : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            IF NOT EXISTS(SELECT Id FROM AspNetRoles WHERE Id = '6f4990f0-1553-44a1-8d9c-7200c4234090')
            BEGIN
            INSERT INTO AspNetRoles(Id, [Name], NormalizedName) VALUES ('6f4990f0-1553-44a1-8d9c-7200c4234090', 'admin', 'ADMIN');
            END

            ");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE AspNetRoles WHERE Id = '6f4990f0-1553-44a1-8d9c-7200c4234090'");
        }
    }
}

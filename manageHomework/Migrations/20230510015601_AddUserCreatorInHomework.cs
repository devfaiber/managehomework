﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace manageHomework.Migrations
{
    /// <inheritdoc />
    public partial class AddUserCreatorInHomework : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserCreatorId",
                table: "Homework",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Homework_UserCreatorId",
                table: "Homework",
                column: "UserCreatorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Homework_AspNetUsers_UserCreatorId",
                table: "Homework",
                column: "UserCreatorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Homework_AspNetUsers_UserCreatorId",
                table: "Homework");

            migrationBuilder.DropIndex(
                name: "IX_Homework_UserCreatorId",
                table: "Homework");

            migrationBuilder.DropColumn(
                name: "UserCreatorId",
                table: "Homework");
        }
    }
}

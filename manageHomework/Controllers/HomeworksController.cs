﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using manageHomework.Entities;
using manageHomework.Models.DTO;
using manageHomework.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace manageHomework.Controllers
{
    [Route("api/homeworks")]
    public class HomeworksController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public HomeworksController(ApplicationDbContext context, IUserService userService, IMapper mapper)
        {
            this._context = context;
            this.userService = userService;
            this.mapper = mapper;
        }

        public async Task<ActionResult<IEnumerable<HomeworkDTO>>> Get()
        {

            /* selecciona las columnas del select sql sin embargo debe retornarse el valor IActionResult usando Ok(result) ya que seria una lista de objetos anonimos
            .Select(t => new
                {
                    t.Id, t.Title
                })
            */

            var userId = userService.GetUserId();
            var homeworks = await _context.Homeworks
                .Where(t=>t.UserCreatorId == userId) // hace un filtro de busqueda
                .OrderBy(t=>t.Order) // ordena por la columna
                /*.Select(t => new
                {
                    t.Id,
                    t.Title
                })*/

                /*.Select(t => new HomeworkDTO
                {
                    Id = t.Id,
                    Title = t.Title,
                }) - mediante un dto */
                
                .ProjectTo<HomeworkDTO>(mapper.ConfigurationProvider) // mediante automapper
                
                
                
                .ToListAsync(); // trae la lista
            return homeworks;
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Homework>> Get(int id)
        {
            var userId = userService.GetUserId();

            var homework = await _context.Homeworks
                .Include(t=>t.Steps.OrderBy(p=>p.Order))
                .Include(t=>t.AttachedFiles.OrderBy(p=>p.Order))
                .FirstOrDefaultAsync(t => t.Id == id && t.UserCreatorId == userId);

            if(homework == null)
            {
                return NotFound();
            }
            return homework;

        }

        [HttpPost]
        public async Task<ActionResult<Homework>> Post([FromBody] string title)
        {
            var userId = userService.GetUserId();
            var existHomeworks = await _context.Homeworks.AnyAsync(t=>t.UserCreatorId == userId);
            var order = 0;

            if(existHomeworks)
            {
                order = await _context.Homeworks.Where(t => t.UserCreatorId == userId)
                    .Select(t=>t.Order).MaxAsync();
            }

            var homework = new Homework
            {
                Title = title,
                Description = "",
                UserCreatorId = userId,
                CreationDate = DateTime.UtcNow,
                Order = order + 1
            };

            _context.Add(homework);
            await _context.SaveChangesAsync();
            return homework;
        }
        [HttpPost("ordering")]
        public async Task<IActionResult> Ordering([FromBody] int[] ids)
        {
            var userId = userService.GetUserId();
            var homeworks = await _context.Homeworks.Where(t => t.UserCreatorId == userId).ToListAsync();
            var homeworksIds = homeworks.Select(t => t.Id);


            var idsNotAllowedToUser = ids.Except(homeworksIds);
            if(idsNotAllowedToUser.Any())
            {
                return Forbid();
            }

            var homeworksDic = homeworks.ToDictionary(h => h.Id);

            for(int i = 0; i < ids.Length; i++)
            {
                var id = ids[i];
                var homework = homeworksDic[id];
                homework.Order = i + 1;
            }
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Put(int id, [FromBody] HomeworkEditDTO request)
        {
            var userId = userService.GetUserId();

            var homework = await _context.Homeworks.FirstOrDefaultAsync(t => t.UserCreatorId == userId && t.Id == id);

            if(homework == null)
            {
                return NotFound();
            }

            homework.Title = request.Title;
            homework.Description = request.Description;

            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            var userId = userService.GetUserId();

            var homework = await _context.Homeworks.FirstOrDefaultAsync(t => t.Id == id && t.UserCreatorId == userId);

            if(homework == null)
            {
                return NotFound();
            }

            _context.Remove(homework);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}

﻿using manageHomework.Models;
using manageHomework.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace manageHomework.Controllers
{
    public class UsersController : Controller
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly ApplicationDbContext applicationDbContext;

        public UsersController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, ApplicationDbContext applicationDbContext)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.applicationDbContext = applicationDbContext;
        }

        [AllowAnonymous]
        public IActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SignUp(SignUpViewModel request)
        {
            if(!ModelState.IsValid)
            {
                return View(request);
            }
            var user = new IdentityUser
            {
                Email = request.Email,
                UserName = request.Email
            };
            var result = await userManager.CreateAsync(user, password: request.Password);

            if (result.Succeeded)
            {
                await signInManager.SignInAsync(user, isPersistent: true);
                return RedirectToAction("Index", "Home");
            }

            foreach(var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }

            return View(request);
        }

        [AllowAnonymous]
        public IActionResult Login(string? message = null)
        {
            if(message != null)
            {
                ViewData["message"] = message;
            }

            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if(!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, isPersistent: true, lockoutOnFailure: false);

            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }

            ModelState.AddModelError(string.Empty, "Nombre usuario o password incorrecto");
            return View(model);
        }
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            return RedirectToAction("Index", "Home");
        }
        [AllowAnonymous]
        [HttpGet]
        public ChallengeResult ExternalLogin(string provider, string returnUrl)
        {
            var urlRedirect = Url.Action("RegisterExternalUser", values: new { returnUrl });
            var externalProperties = signInManager.ConfigureExternalAuthenticationProperties(provider, urlRedirect);

            return new ChallengeResult(provider, externalProperties);
        }
        [AllowAnonymous]
        public async Task<IActionResult> RegisterExternalUser(string returnUrl, string remoteError = null)
        {
            returnUrl = string.IsNullOrEmpty(returnUrl) ? "/" : returnUrl;
            var message = "";
            if(remoteError != null)
            {
                message = $"Error remote: {remoteError}";
                return RedirectToAction("login", routeValues: new { message });

            }

            var info = await signInManager.GetExternalLoginInfoAsync();
            if(info == null)
            {
                message = "Error data external nor found!";
                return RedirectToAction("login", routeValues: new { message });
            }

            var externalResult = await signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: true, bypassTwoFactor: true);

            if (externalResult.Succeeded)
            {
                return LocalRedirect(returnUrl);
            }

            string email = "";
            if (info.Principal.HasClaim(p => p.Type == ClaimTypes.Email))
            {
                email = info.Principal.FindFirstValue(ClaimTypes.Email);
            } else
            {
                message = "Error al extraer el email de usuario externo";
                return RedirectToAction("login", routeValues: new { message });
            }

            var user = new IdentityUser { Email = email, UserName = email };

            var resultCreateUser = await userManager.CreateAsync(user);

            if (!resultCreateUser.Succeeded)
            {
                message = resultCreateUser.Errors.First().Description;
                return RedirectToAction("login", routeValues: new { message });
            }

            var resultAddLoginExternal = await userManager.AddLoginAsync(user, info);

            if(resultAddLoginExternal.Succeeded)
            {
                await signInManager.SignInAsync(user, isPersistent: true, info.LoginProvider);
                return LocalRedirect(returnUrl);
            }
            message = "Error al agregar el login";
            return RedirectToAction("login", routeValues: new { message });
        }

        [Authorize(Roles = Constants.ROLADMIN)]
        [HttpGet]
        public async Task<IActionResult> List(string? message = null)
        {
            var users = await applicationDbContext.Users.Select(u=>new UserViewModel
            {
                Email = u.Email!
            }).ToListAsync();

            var userViewModel = new UserListViewModel();
            userViewModel.Users = users;
            userViewModel.Message = message;
            return View(userViewModel);
        }
        [Authorize(Roles = Constants.ROLADMIN)]
        [HttpPost]
        public async Task<IActionResult> DoAdmin(string email)
        {
            var user = await applicationDbContext.Users.Where(u=>u.Email == email).FirstOrDefaultAsync();

            if(user == null)
            {
                return NotFound();
            }

            await userManager.AddToRoleAsync(user, Constants.ROLADMIN);


            return RedirectToAction(nameof(List), routeValues: new { message = "Rol Asignado correctamente del email " + email });
        }
        [Authorize(Roles = Constants.ROLADMIN)]
        [HttpPost]
        public async Task<IActionResult> RemoveAdmin(string email)
        {
            var user = await applicationDbContext.Users.Where(u => u.Email == email).FirstOrDefaultAsync();

            if (user == null)
            {
                return NotFound();
            }

            await userManager.RemoveFromRoleAsync(user, Constants.ROLADMIN);


            return RedirectToAction(nameof(List), routeValues: new { message = "Rol removido correctamente del email " + email });
        }
    }
}

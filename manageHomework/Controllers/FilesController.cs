﻿using manageHomework.Entities;
using manageHomework.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Runtime.InteropServices;

namespace manageHomework.Controllers
{
    [Route("api/files")]
    public class FilesController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IStoreFile storeFile;
        private readonly IUserService userService;
        private readonly string container = "archivosAdjuntos";
        public FilesController(ApplicationDbContext context, IStoreFile storeFile, IUserService userService)
        {
            this.context = context;
            this.storeFile = storeFile;
            this.userService = userService;
        }

        [HttpPost("{homeworkId:int}")]
        public async Task<ActionResult<IEnumerable<AttachedFile>>> Post(int homeworkId, [FromForm] IEnumerable<IFormFile> files)
        {
            var userId = userService.GetUserId();
            var homework = await context.Homeworks.FirstOrDefaultAsync(t=>t.Id == homeworkId);

            if (homework == null)
            {
                return NotFound();
            } else if(homework.UserCreatorId != userId)
            {
                return Forbid();
            }

            var existAttachments = await context.AttachedFiles.AnyAsync(a=>a.HomeworkId == homeworkId);

            var orderMax = 0;
            if(existAttachments)
            {
                orderMax = await context.AttachedFiles.Where(a => a.HomeworkId == homeworkId).Select(a => a.Order).MaxAsync();
            }

            var results = await storeFile.SaveAsync(container, files);

            var attachments = results.Select((result, index) => new AttachedFile { 
                HomeworkId = homeworkId, 
                CreatedDate = DateTime.UtcNow, 
                Url = result.Url,
                Title = result.Title,
                Order = orderMax + index + 1,
            }).ToList();


            context.AddRange(attachments);
            await context.SaveChangesAsync();
            return attachments;
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] string title)
        {
            var userId = userService.GetUserId();
            var attachment = await context.AttachedFiles.Include(a => a.HomeworkItems).FirstOrDefaultAsync(a => a.Id == id);
            if (attachment == null)
            {
                return NotFound();
            }
            else if(attachment.HomeworkItems.UserCreatorId != userId)
            {
                return Forbid();
            }

            attachment.Title = title;
            await context.SaveChangesAsync();
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var userId = userService.GetUserId();
            var attachment = await context.AttachedFiles.Include(a => a.HomeworkItems).FirstOrDefaultAsync(a => a.Id == id);
            if (attachment == null)
            {
                return NotFound();
            }
            else if (attachment.HomeworkItems.UserCreatorId != userId)
            {
                return Forbid();
            }

            context.Remove(attachment);
            await context.SaveChangesAsync();
            await storeFile.DeleteAsync(attachment.Url, container);
            return Ok();
        }
    }
}

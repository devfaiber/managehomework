﻿using manageHomework.Entities;
using manageHomework.Models.DTO;
using manageHomework.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace manageHomework.Controllers
{
    [Route("api/steps")]
    public class StepsController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IUserService userService;

        public StepsController(ApplicationDbContext context, IUserService userService)
        {
            this.context = context;
            this.userService = userService;
        }

        [HttpPost("{homeworkId:int}")]
        public async Task<ActionResult<Step>> Post(int homeworkId, [FromBody] StepCreateDTO request)
        {
            var userId = userService.GetUserId();

            var homework = await context.Homeworks.FirstOrDefaultAsync(t => t.Id == homeworkId);

            if (homework == null)
            {
                return NotFound();
            }
            else if (homework.UserCreatorId != userId)
            {
                return Forbid();
            }

            var existsSteps = await context.Steps.AnyAsync(s => s.HomeworkId == homeworkId);
            int orderMax = 0;
            if (existsSteps)
            {
                orderMax = await context.Steps.Where(s => s.HomeworkId == homeworkId).Select(s => s.Order).MaxAsync();
            }

            var step = new Step
            {
                HomeworkId = homeworkId,
                Order = orderMax + 1,
                Description = request.Description,
                IsCompleted = request.Completed
            };

            context.Add(step);
            await context.SaveChangesAsync();
            return step;
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(Guid id, [FromBody] StepCreateDTO request)
        {
            var userId = userService.GetUserId();
            var step = await context.Steps.Include(s => s.HomeworkItems).FirstOrDefaultAsync(s => s.Id == id);

            if (step == null) { return NotFound(); }
            if (step.HomeworkItems.UserCreatorId != userId) { return Forbid(); }

            step.Description = request.Description;
            step.IsCompleted = request.Completed;

            await context.SaveChangesAsync();
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var userId = userService.GetUserId();
            var step = await context.Steps.Include(s => s.HomeworkItems).FirstOrDefaultAsync(s => s.Id == id);

            if (step == null) { return NotFound(); }
            if (step.HomeworkItems.UserCreatorId != userId) { return Forbid(); }

            context.Remove(step);
            await context.SaveChangesAsync();
            return Ok();
        }

        [HttpPost("order/{homeworkId:int}")]
        public async Task<IActionResult> Ordering(int homeworkId, [FromBody] Guid[] ids)
        {
            var userId = userService.GetUserId();
            var homework = await context.Homeworks.FirstOrDefaultAsync(h => h.Id == homeworkId && h.UserCreatorId == userId);
            if (homework == null)
            {
                return NotFound();
            }
            var steps = await context.Steps.Where(x => x.HomeworkId == homeworkId).ToListAsync();
            var stepIds = steps.Select(s => s.Id);

            var idsNotAllowed = ids.Except(stepIds);
            if(idsNotAllowed.Any())
            {
                return BadRequest("Faltas pasos por incluir");
            }

            var stepsDic = steps.ToDictionary(s => s.Id);

            for(int i = 0; i < ids.Length; i++)
            {
                var stepId = ids[i];
                var step = stepsDic[stepId];
                step.Order = i + 1;
            }
            await context.SaveChangesAsync();
            return Ok();
        }

    }
}

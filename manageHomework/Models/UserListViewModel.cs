﻿namespace manageHomework.Models
{
    public class UserListViewModel
    {
        public IList<UserViewModel> Users { get; set; }
        public string? Message { get; set; }
    }
}

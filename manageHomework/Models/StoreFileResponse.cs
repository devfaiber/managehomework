﻿namespace manageHomework.Models
{
    public class StoreFileResponse
    {
        public string Url { get; set; }
        public string Title { get; set; }
    }
}

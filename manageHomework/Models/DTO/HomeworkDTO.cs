﻿namespace manageHomework.Models.DTO
{
    public class HomeworkDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int TotalCompletedSteps { get; set; }
        public int TotalSteps { get; set; }
    }
}

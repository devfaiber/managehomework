﻿using System.ComponentModel.DataAnnotations;

namespace manageHomework.Models.DTO
{
    public class HomeworkEditDTO
    {
        [Required]
        [StringLength(250)]
        public string Title { get; set; }
        public string Description { get; set; }
    }
}

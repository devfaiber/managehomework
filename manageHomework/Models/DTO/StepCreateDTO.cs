﻿namespace manageHomework.Models.DTO
{
    public class StepCreateDTO
    {
        public string Description { get; set; }
        public bool Completed { get; set; }
    }
}

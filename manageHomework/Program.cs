using manageHomework;
using manageHomework.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

var policyUserAuthenticated = new AuthorizationPolicyBuilder()
    .RequireAuthenticatedUser()
    .Build();

// Add services to the container.
builder.Services.AddControllersWithViews(options =>
{
    options.Filters.Add(new AuthorizeFilter(policyUserAuthenticated));
}).AddViewLocalization(Microsoft.AspNetCore.Mvc.Razor.LanguageViewLocationExpanderFormat.Suffix)
.AddDataAnnotationsLocalization(options =>
{
    options.DataAnnotationLocalizerProvider = (_, factory) => factory.Create(typeof(SharedResource));
}).AddJsonOptions(options =>
{
    /*
    ignora referencia ciclica cuando se retorna una entidad que posee referencia a otro entidad y a su vez esta entidad vuelve a hacer referencia a la que se retorna. como una opcion de referencia de doble canal.

    eje: Step hace referencia a Homework y Homework hace referencia a la lista de Step. por tanto a retornar step se tiene los Step y cada step apunta a tarea y asi sucesivamente.
    */
    options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
});

builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer("name=DefaultConnection"));

builder.Services
    .AddAuthentication()
    .AddMicrosoftAccount(options =>
    {
        options.ClientId = builder.Configuration["MicrosoftClientId"]!;
        options.ClientSecret = builder.Configuration["MicrosoftSecretId"]!;
    });

builder.Services.AddIdentity<IdentityUser, IdentityRole>(options =>
{
    options.SignIn.RequireConfirmedAccount = false;
    options.Password.RequireNonAlphanumeric = false;
    options.Password.RequiredLength = 3;
    options.Password.RequireDigit = false;
    options.Password.RequireLowercase = false;
    options.Password.RequireUppercase = false;

}).AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();

// no trabajar con vistas de identity. usare las propias
builder.Services.PostConfigure<CookieAuthenticationOptions>(IdentityConstants.ApplicationScheme, options =>
{
    options.LoginPath = "/users/login";
    options.AccessDeniedPath = "/users/login";
});

builder.Services.AddLocalization( options =>
{
    // ruta de los resources con la estructura
    // Resources > Controllers > {nombrecontroller}.{lenguaje}.resx
    options.ResourcesPath = "Resources";
});

builder.Services.AddTransient<IUserService, UserService>();
builder.Services.AddAutoMapper(typeof(Program));
builder.Services.AddTransient<IStoreFile, AzureStorageService>();

var app = builder.Build();

// soportar diferente lenguajes

app.UseRequestLocalization(options =>
{
    options.DefaultRequestCulture = new RequestCulture("es");
    options.SupportedUICultures = Constants.CulturasUISupports.Select(e => new CultureInfo(e.Value)).ToList();
});

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

﻿function manageClickAddStep() {

    const index = homeworkEditViewModel.steps().findIndex(s => s.isNew());
    if (index !== -1) {
        return;
    }

    homeworkEditViewModel.steps.push(new stepViewModelFun({
        modeEdition: true, completed: false
    }));

    $('[name=txtStepDescription]:visible').focus();
}

function manageClickCancelStep(step) {
    if (step.isNew()) {
        homeworkEditViewModel.steps.pop();
    } else {
        step.modeEdition(false);
        step.description(step.oldDescription);
    }
}
function manageClickSaveStep(step) {
    step.modeEdition(false);
    const isNew = step.isNew();
    const homeworkId = homeworkEditViewModel.id;
    const data = getRequestBodyStep(step);
    const description = step.description();


    if (!description) {
        step.description(step.oldDescription);
        if (isNew) {
            homeworkListViewModel.steps.pop();
        }
        return;
    }

    if (isNew) {
        insertStep(step, data, homeworkId);
    } else {
        updateStep(data, step.id());
    }
}

async function insertStep(step, data, homeworkId) {
    const response = await fetch(`${urlSteps}/${homeworkId}`, {
        body: data,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (response.ok) {
        const json = await response.json();
        step.id(json.id);
        const homework = getHomeworkInEdition();
        homework.totalSteps(homework.totalSteps() + 1);
        if (step.completed()) {
            homework.totalCompletedSteps(homework.totalCompletedSteps() + 1);
        }
    } else {
        manageErrorApi(response);

    }
}

function getRequestBodyStep(step) {
    return JSON.stringify({
        description: step.description(),
        completed: step.completed()
    });
}

function manageClickDescriptionStep(step) {
    step.modeEdition(true);
    step.oldDescription = step.description();
    $('[name=txtStepDescription]:visible').focus();

}
async function updateStep(data, id) {
    const response = await fetch(`${urlSteps}/${id}`, {
        body: data,
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (!response.ok) {
        manageErrorApi(response);
    }
}
function manageClickCheckboxStep(step) {


    if (step.isNew()) {
        return true;
    }

    const data = getRequestBodyStep(step);
    updateStep(data, step.id());

    const homework = getHomeworkInEdition();
    let stepCurrentCompleted = homework.totalCompletedSteps();
    if (step.completed()) {
        stepCurrentCompleted++;
    } else {
        stepCurrentCompleted--;
    }
    homework.totalCompletedSteps(stepCurrentCompleted);


    return true;
}
function manageClickDeleteStep(step) {
    modalEditBootstrap.hide();
    confirmAction({
        callbackAccept: () => {
            deleteStep(step);
            modalEditBootstrap.show();
            const homework = getHomeworkInEdition();
            homework.totalSteps(homework.totalSteps() - 1);
            if (step.completed()) {
                homework.totalCompletedSteps(homework.totalCompletedSteps() - 1);
            }
        },
        callbackCancel: () => {
            modalEditBootstrap.show();
        },
        title: `Desea eliminar este paso?`
    });

    
}

async function deleteStep(step) {
    const response = await fetch(`${urlSteps}/${step.id()}`, {
        method: 'DELETE',
    });

    if (!response.ok) {
        manageErrorApi(response);
        return;
    }

    homeworkEditViewModel.steps.remove(function (item) {
        return item.id() == step.id()
    });
}


async function updateOrderSteps() {
    const ids = getIdsSteps();

    await sendIdsToBackend(ids);
    const arrayOrdering = homeworkEditViewModel.steps.sorted(function (a, b) {
        return ids.indexOf(a.id().toString()) - ids.indexOf(b.id().toString());
    });
    homeworkEditViewModel.steps(arrayOrdering);
}

async function sendIdsToBackend(ids) {
    var data = JSON.stringify(ids);
    await fetch(`${urlSteps}/order/${homeworkEditViewModel.id}`, {
        method: 'POST',
        body: data,
        headers: {
            'Content-Type': 'application/json'
        }
    });


}

function getIdsSteps() {
    const ids = $("[name=chbStep]").map(function () {
        return $(this).attr('data-id');
    }).get();

    return ids;
}

$(function () {
    $("#reordenable-steps").sortable({
        axis: 'y',
        stop: async function () {
            await updateOrderSteps();
        }
    })

})
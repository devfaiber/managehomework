﻿async function manageErrorApi(response) {
    let messageError = "";

    if (response.status == 400) {
        messageError = await response.text();
    } else if (response.status == 404) {
        messageError = resResourceNotFound;
    } else {
        messageError = resRequestError;
    }
    showMessageError(messageError);
}

function showMessageError(message) {
    Swal.fire({
        icon: 'error',
        title: 'Error',
        text: message
    });
}
function confirmAction({ callbackAccept, callbackCancel, title }) {
    Swal.fire({
        title: title || "Desea confimar esta accion?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        focusConfirm: true
    }).then(result => {
        if (result.isConfirmed) {
            callbackAccept();
        } else if (callbackCancel) {
            callbackCancel();
        }
    });
}

function downloadFile(url, name) {
    var link = document.createElement('a');
    link.download = name;
    link.target = '_blank';
    link.href = url;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    delete link;
}
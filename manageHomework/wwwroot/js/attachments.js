﻿let inputAttachment = document.querySelector("#attachments-input");

function manageClickAddAttachment() {
    inputAttachment.click();
}
async function manageSelectFiles(event) {
    const files = event.target.files;
    const arrayFiles = Array.from(files);

    const idHomework = homeworkEditViewModel.id;
    const formData = new FormData();

    for (let i = 0; i < arrayFiles.length; i++) {
        formData.append("files", arrayFiles[i]);
    }

    const response = await fetch(`${urlFiles}/${idHomework}`, {
        body: formData,
        method: 'POST'
    });

    if (!response.ok) {
        manageErrorApi(response);
        return;
    }
    const json = await response.json();
    prepareAttachment(json)
    inputAttachment.value = null;
}

function prepareAttachment(attachments) {
    attachments.forEach(item => {
        let createdDate = item.createdDate;
        if (item.createdDate.indexOf('Z') === -1) {
            createdDate += 'Z';
        }

        const createdDateDT = new Date(createdDate);
        item.publish = createdDateDT.toLocaleString();

        homeworkEditViewModel.attachments.push(new attachmentViewModel({
            ...item, modeEdition: false
        }));
    });
}
let titleOld;
function manageClickTitleAttachment(attachment) {
    attachment.modeEdition(true);
    titleOld = attachment.title();

    $("[name=txtAttachmentTitle]:visible").focus();
}
async function manageFocusTitleAttachment(attachment) {
    attachment.modeEdition(false);
    const id = attachment.id;
    if (!attachment.title()) {
        attachment.title(titleOld);
    }
    if (attachment.title() === titleOld) {
        return;
    }

    const data = JSON.stringify(attachment.title());
    const response = await fetch(`${urlFiles}/${id}`, {
        body: data,
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (!response.ok) {
        manageErrorApi(response);
    }

}
function manageDeleteAttachment(attachment) {
    modalEditBootstrap.hide();

    confirmAction({
        callbackAccept: () => {
            deleteAttachment(attachment);
            modalEditBootstrap.show();
        },
        callbackCancel: () => {
            modalEditBootstrap.show();
        },
        title: 'Desea eliminar este archivo?'
    })
}
async function deleteAttachment(attachment) {
    const response = await fetch(`${urlFiles}/${attachment.id}`, {
        method: 'DELETE'
    });

    if (!response.ok) {
        manageErrorApi(response);
        return;
    }

    homeworkEditViewModel.attachments.remove(item => {
        return item.id == attachment.id
    });

}
function manageDownloadAttachment(attachment) {
    downloadFile(attachment.url, attachment.title());
}
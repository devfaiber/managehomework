﻿function addNewHomework() {
    homeworkListViewModel.homeworks.push(new homeworkItemViewModel({ id: 0, title: "" }));

    $('[name=title-homework]').last().focus();
}

async function onFocusOutHomework(homework) {
    const title = homework.title();
    if (!title) {
        homeworkListViewModel.homeworks.pop();
        return;
    }

    const data = JSON.stringify(title);
    const response = await fetch(`${urlHomeworks}`, {
        method: 'POST',
        body: data,
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (response.ok) {
        const json = await response.json();
        homework.id(json.id);
    } else {
        manageErrorApi(response);
    }
}
async function GetHomeworks() {
    homeworkListViewModel.loading(true);

    const response = await fetch(`${urlHomeworks}`);

    if (!response.ok) {
        homeworkListViewModel.loading(false);
        manageErrorApi(response);
        return;
    }
    const json = await response.json();
    homeworkListViewModel.homeworks([]);

    json.forEach(value => {
        homeworkListViewModel.homeworks.push(new homeworkItemViewModel(value));
    });

    homeworkListViewModel.loading(false);
}

async function updateOrderHomeworks() {
    const ids = getIdsHomeworks();
    await sendIdsHomeworkToBacked(ids);


    const arrayOrdering = homeworkListViewModel.homeworks.sorted(function (a, b) {
        return ids.indexOf(a.id().toString()) - ids.indexOf(b.id().toString())
    })

    homeworkListViewModel.homeworks([]);
    homeworkListViewModel.homeworks(arrayOrdering);

}
function getIdsHomeworks() {
    const ids = $("[name=title-homework]").map(function () {
        return $(this).attr('data-id');
    }).get(); // .get convierte el objeto obtenido en un array
    return ids;
}

async function sendIdsHomeworkToBacked(ids) {
    var data = JSON.stringify(ids);

    await fetch(`${urlHomeworks}/ordering`, {
        method: 'POST',
        body: data,
        headers: {
            'Content-Type': 'application/json'
        }
    });
}

async function onClickHomework(homework) {
    if (homework.isNew()) {
        return;
    }

    const response = await fetch(`${urlHomeworks}/${homework.id()}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (!response.ok) {
        manageErrorApi(response);
        return;
    }
    const json = await response.json();

    homeworkEditViewModel.id = json.id;
    homeworkEditViewModel.title(json.title);
    homeworkEditViewModel.description(json.description);
    homeworkEditViewModel.steps([]);
    json.steps.forEach(step => {
        homeworkEditViewModel.steps.push(
            new stepViewModelFun({
                id: step.id,
                description: step.description,
                completed: step.isCompleted,
                modeEdition: false
            })
        );
    });
    homeworkEditViewModel.attachments([]);
    prepareAttachment(json.attachedFiles);

    modalEditBootstrap.show();
}

async function onchageEditHomework() {
    const data = {
        id: homeworkEditViewModel.id,
        title: homeworkEditViewModel.title(),
        description: homeworkEditViewModel.description()
    };

    if (!data.title) {
        return;
    }

    await editHomeworkComplete(data);

    const homework = homeworkListViewModel.homeworks().find(h => h.id() == data.id);

    homework.title(data.title);

}

async function editHomeworkComplete(homework) {
    const data = JSON.stringify(homework);

    const response = await fetch(`${urlHomeworks}/${homework.id}`, {
        method: 'PUT',
        body: data,
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (!response.ok) {
        manageErrorApi(response);
        throw new Error("error!!");
    }
}

function tryDeleteHomework(homework) {
    modalEditBootstrap.hide();

    confirmAction({
        callbackAccept: () => {
            deleteHomework(homework);
        },
        callbackCancel: () => {
            modalEditBootstrap.show();
        },
        title: `Desea eliminar la tarea ${homework.title()}`
    });
}

async function deleteHomework(homework) {
    const id = homework.id;
    const response = await fetch(`${urlHomeworks}/${id}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (!response.ok) {
        manageErrorApi(response);
        return;
    }
    const index = findIndexHomeworkById(id);
    homeworkListViewModel.homeworks.splice(index, 1);
}

function findIndexHomeworkById(id) {
    return homeworkListViewModel.homeworks().findIndex(h => h.id() == id);
}

function getHomeworkInEdition() {
    index = findIndexHomeworkById(homeworkEditViewModel.id);
    return homeworkListViewModel.homeworks()[index];
}

$(function () {
    $("#reordenable").sortable({
        axis: 'y',
        stop: async function () {
            await updateOrderHomeworks();
        }
    });
})
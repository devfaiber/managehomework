﻿using System.ComponentModel.DataAnnotations.Schema;

namespace manageHomework.Entities
{
    public class Step
    {
        public Guid Id { get; set; }

        //[ForeignKey(nameof(HomeworkItems))] // HomeworkId seria la convecion. sin convencion se debe definir un atributo que apunte a la propiedad de navegacion
        public int HomeworkId { get; set; }
        public string Description { get; set; }
        public bool IsCompleted { get; set; }
        public int Order { get; set; }

        // property navegation
        [ForeignKey(nameof(HomeworkId))] // apunta teniendo en cuenta la propiedad
        public Homework HomeworkItems { get; set; }
    }
}

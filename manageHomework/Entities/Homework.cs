﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace manageHomework.Entities
{
    [Table("Homework")] // nombre de la tabla
    public class Homework
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(250)]
        [Required]
        public string Title { get; set; }
        public string? Description { get; set; }
        public int Order { get; set; }
        public DateTime CreationDate { get; set; }
        public string UserCreatorId { get; set; }

        // property navigation
        public IdentityUser UserCreator { get; set; }
        public List<Step> Steps { get; set; }
        public List<AttachedFile> AttachedFiles { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace manageHomework.Entities
{
    public class AttachedFile
    {
        public Guid Id { get; set; }
        public int HomeworkId { get; set; }
        [Unicode]
        public string Url { get; set; }
        public string Title { get; set; }
        public int Order { get; set; }
        public DateTime CreatedDate { get; set; }

        [ForeignKey(nameof(HomeworkId))]
        public Homework HomeworkItems { get; set; }

    }
}

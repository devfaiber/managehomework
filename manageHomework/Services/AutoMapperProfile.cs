﻿using AutoMapper;
using manageHomework.Entities;
using manageHomework.Models.DTO;

namespace manageHomework.Services
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Homework, HomeworkDTO>()
                .ForMember(dto => dto.TotalSteps, ent => ent.MapFrom(x => x.Steps.Count()))
                .ForMember(dto => dto.TotalCompletedSteps, ent => ent.MapFrom(x => x.Steps.Where(s => s.IsCompleted).Count()));


        }
    }
}

﻿using System.Security.Claims;

namespace manageHomework.Services
{
    public interface IUserService
    {
        string GetUserId();
    }
    public class UserService : IUserService
    {
        private HttpContext httpContext;

        public UserService(IHttpContextAccessor httpContextAccessor)
        {
            httpContext = httpContextAccessor.HttpContext!;
        }

        public string GetUserId()
        {
            if (httpContext.User.Identity.IsAuthenticated)
            {
                var claimId = httpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)!;
                return claimId.Value;
            }

            throw new Exception("user not authenticated!");
        }
    }
}

﻿using manageHomework.Migrations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace manageHomework.Services
{
    public sealed class Constants
    {
        public const string ROLADMIN = "admin";

        public static readonly SelectListItem[] CulturasUISupports = new SelectListItem[]
        {
            new SelectListItem(value: "es", text: "Espanhol"),
            new SelectListItem(value: "en", text: "English")
        };
    }
}

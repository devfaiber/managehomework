﻿using manageHomework.Models;

namespace manageHomework.Services
{
    public interface IStoreFile
    {
        Task DeleteAsync(string path, string container);
        /// <summary>
        /// guarda el archivo
        /// </summary>
        /// <param name="container"> simplemente la carpeta donde se va a guardar</param>
        /// <param name="files"> lista de archivos que va a guardar.</param>
        /// <returns></returns>
        Task<IEnumerable<StoreFileResponse>> SaveAsync(string container, IEnumerable<IFormFile> files);
    }
}

﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using manageHomework.Models;

namespace manageHomework.Services
{
    public class AzureStorageService: IStoreFile
    {
        private readonly string connectionString;
        public AzureStorageService(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("AzureStorage")!;
        }

        public async Task DeleteAsync(string path, string container)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                return;
            }
            var client = new BlobContainerClient(connectionString, container.ToLower());
            await client.CreateIfNotExistsAsync();
            var nameFile = Path.GetFileName(path);
            var blob = client.GetBlobClient(nameFile);
            await blob.DeleteIfExistsAsync();
        }

        public async Task<IEnumerable<StoreFileResponse>> SaveAsync(string container, IEnumerable<IFormFile> files)
        {
            var client = new BlobContainerClient(connectionString, container.ToLower());
            await client.CreateIfNotExistsAsync();
            client.SetAccessPolicy(PublicAccessType.Blob);
            var homeworks = files.Select(async file =>
            {
                var nameOriginal = Path.GetFileName(file.FileName);
                var ext = Path.GetExtension(file.FileName);
                var newName = $"{Guid.NewGuid()}{ext}";

                var blob = client.GetBlobClient(newName);
                var blobHttpHeaders = new BlobHttpHeaders();
                blobHttpHeaders.ContentType = file.ContentType;

                await blob.UploadAsync(file.OpenReadStream(), blobHttpHeaders);

                return new StoreFileResponse
                {
                    Url = blob.Uri.ToString(),
                    Title = nameOriginal
                };
            });
            var results = await Task.WhenAll(homeworks);
            return results;
        }
    }
}

﻿using manageHomework.Models;

namespace manageHomework.Services
{
    public class LocalStorageService : IStoreFile
    {
        private readonly IWebHostEnvironment env;
        private readonly HttpContext context;

        public LocalStorageService(IWebHostEnvironment env, IHttpContextAccessor httpContextAccessor)
        {
            this.env = env;
            this.context = httpContextAccessor.HttpContext!;
        }

        public Task DeleteAsync(string path, string container)
        {
            if(string.IsNullOrEmpty(path))
            {
                return Task.CompletedTask;
            }

            var nameFile = Path.GetFileName(path);
            var folderFile = Path.Combine(env.WebRootPath, container, nameFile);

            if(File.Exists(folderFile))
            {
                File.Delete(folderFile);
            }
            return Task.CompletedTask;
        }

        public async Task<IEnumerable<StoreFileResponse>> SaveAsync(string container, IEnumerable<IFormFile> files)
        {
            var homeworks = files.Select(async file =>
            {
                var nameOriginal = Path.GetFileName(file.FileName);
                var ext = Path.GetExtension(file.FileName);
                var newName = $"{Guid.NewGuid()}{ext}";
                string folder = Path.Combine(env.WebRootPath, container);

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                string route = Path.Combine(folder, newName);
                using (var ms = new MemoryStream())
                {
                    await file.CopyToAsync(ms);
                    var content = ms.ToArray();
                    await File.WriteAllBytesAsync(route, content);
                }

                var url = $"{context.Request.Scheme}://{context.Request.Host}";
                var urlFile = Path.Combine(url, container, newName).Replace("\\", "/");

                return new StoreFileResponse
                {
                    Url = urlFile,
                    Title = nameOriginal
                };
            });

            var results = await Task.WhenAll(homeworks);
            return results;

        }
    }
}

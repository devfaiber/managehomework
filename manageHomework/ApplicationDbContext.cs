﻿using manageHomework.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace manageHomework
{
    public class ApplicationDbContext: IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions options): base(options) { }

        public DbSet<Homework> Homeworks { get; set; }

        public DbSet<Step> Steps { get; set; }
        public DbSet<AttachedFile> AttachedFiles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //- aplica cambios a una propiedad
            //modelBuilder.Entity<Homework>().Property(t=>t.Title).IsRequired().HasMaxLength(200);
        }
    }
}
